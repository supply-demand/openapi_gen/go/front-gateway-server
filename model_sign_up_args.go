/*
 * Front gateway
 *
 * Front gateway
 *
 * API version: 0.0.1
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi

type SignUpArgs struct {

	Email string `json:"email"`

	Password string `json:"password"`
}

// AssertSignUpArgsRequired checks if the required fields are not zero-ed
func AssertSignUpArgsRequired(obj SignUpArgs) error {
	elements := map[string]interface{}{
		"email": obj.Email,
		"password": obj.Password,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseSignUpArgsRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of SignUpArgs (e.g. [][]SignUpArgs), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseSignUpArgsRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aSignUpArgs, ok := obj.(SignUpArgs)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertSignUpArgsRequired(aSignUpArgs)
	})
}
